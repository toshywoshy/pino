<?php

namespace Drupal\member;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\member\Entity\MemberInterface;

/**
 * Defines the storage handler class for Member entities.
 *
 * This extends the base storage class, adding required special handling for
 * Member entities.
 *
 * @ingroup member
 */
class MemberStorage extends SqlContentEntityStorage implements MemberStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(MemberInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {member_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {member_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(MemberInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {member_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('member_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
